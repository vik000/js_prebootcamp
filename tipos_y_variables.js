var numero=1;
console.log(numero);
console.log(typeof(numero));
var numero='1';
console.log(typeof(numero));

console.log("texto de prueba".split(' '));
var textLargo1=
  'linea1\n'+
  'linea2\n';

console.log(textLargo1);

var textLargo2=[
  'linea1',
  'linea2'
].join('\n');

console.log(textLargo2);

var objeto={
  numero:1,
  texto:"hola",
  esCero: function (v){
    return v+2;
  }
};

console.log(objeto.numero);
console.log(objeto.texto);
console.log(objeto.esCero(5));


//array:

var array=[
  1,'hola',function(v) {return v+2},
  {valor:"hola"}
];

console.log(array);
console.log(array[2](5));

//hoisting:
var x=100;
function y(){
  if (x==20){
    var x=30;
  }
  return x;
};

console.log(x,y());
