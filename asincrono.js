"use strict";

console.log('empiezo');

function escribeTras2segundos(texto,callback){
  setTimeout(function(){
    console.log(texto);
    callback();
  },2000);
}

escribeTras2segundos('texto principal de la función',function(){
  console.log('fin del retraso');
});

escribeTras2segundos('texto secundario',function(){
  console.log('fin del retraso en paralelo');
});


escribeTras2segundos('texto principal de la función',function(){
  console.log('fin del retraso');
  escribeTras2segundos('texto secundario',function(){
    console.log('fin del retraso en serie');
  })
});
