"use strict";

console.log('empiezo');

function escribeTras2segundos(texto,callback){
  setTimeout(function(){
    console.log(texto);
    callback();
  },2000);
}

//bucle asíncrono en serie:
//llamar a una función N veces en serie:
//al finalizar llamar al callback de finalización.

function serie(n,func,callbackFin){
  if(n==0){
    callbackFin();
    return;
  }
  n--;
  func(n,function(){
    serie(n,func,callbackFin);
  })
}

serie(5,escribeTras2segundos,function(){
  console.log('he treminado');
})
