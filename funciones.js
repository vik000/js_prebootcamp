//una función básica:
function funcionBasica(){
  console.log('función básica bien ejecutada');
}

funcionBasica();

//función como declaración con argumentos y retorno:
function suma(a,b){
  return a+b;
}

console.log(suma(10,3));

//función como expresión:
var multiplica=function (a,b){
  return a*b;
}
console.log(multiplica(2,4));

var lang="es";
function accesoContexto(){
  console.log('en el contexto superior: ',lang);
}
accesoContexto();

var agente={
  nombre: 'Smith',
  saluda: function(){
    console.log('Hola, soy el agente '+this.nombre);
  }
}

agente.saluda();

//constructor de objetos:
function Fruta(nombre){
  this.getNombre=function(){
    return nombre;
  }
  this.setNombre=function(valor){
    nombre=valor;
  }
}


//crear un objeto fruta:
var limon=new Fruta('Citrus limón');
console.log(limon);

console.log(limon.getNombre());
limon.setNombre('manzana');
console.log(limon.getNombre());
